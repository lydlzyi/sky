/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Commit, ActionTree, MutationTree } from 'vuex';
import * as types from '@/store/mutation-types';
import { AxiosResponse } from 'axios';
import graph from '@/graph';
import { Option } from '@/types/global';
export interface State {
  logs: any[];
  total: number;
  loading: boolean;
  logErrors: { [key: string]: string };
  conditions: any;
}

const metaState: State = {
  logs: [],
  total: 0,
  loading: false,
  logErrors: {},
  conditions: {},
};

// mutations
const mutations: MutationTree<State> = {
  [types.SET_META_LOGS](state: State, data: any[]) {
    state.logs = data;
  },
  [types.SET_META_LOGS_TOTAL](state: State, data: number) {
    state.total = data;
  },
  [types.SET_META_LOADING](state: State, data: boolean) {
    state.loading = data;
  },
  [types.SET_META_LOG_ERRORS](state: State, data: { msg: string; desc: string }) {
    state.logErrors = {
      ...state.logErrors,
      [data.msg]: data.desc,
    };
  },
  [types.SET_META_LOG_CONDITIONS](state: State, item: Option) {
    state.conditions = {
      ...state.conditions,
      [item.label]: item.key,
    };
  },
};

// actions
const actions: ActionTree<State, any> = {
  QUERY_META_LOGS(context: { commit: Commit; state: State }, params: any) {
    context.commit('SET_META_LOADING', true);
    return graph
      .query('queryMetaLogs')
      .params(params)
      .then((res: AxiosResponse<any>) => {
        context.commit(types.SET_LOG_ERRORS, { msg: 'queryMetaLogs', desc: res.data.errors || '' });
        if (res.data && res.data.errors) {
          context.commit('SET_META_LOGS', []);
          context.commit('SET_META_LOGS_TOTAL', 0);
          return;
        }
        context.commit('SET_META_LOGS', res.data.data.queryMetaLogs.list);
        context.commit('SET_META_LOGS_TOTAL', res.data.data.queryMetaLogs.total);
      })
      .finally(() => {
        context.commit('SET_META_LOADING', false);
      });
  },
};

export default {
  state: metaState,
  actions,
  mutations,
};
