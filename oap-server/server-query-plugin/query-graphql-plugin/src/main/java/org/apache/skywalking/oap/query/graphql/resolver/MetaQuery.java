/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.skywalking.oap.query.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.oap.server.core.CoreModule;
import org.apache.skywalking.oap.server.core.query.MetaQueryService;
import org.apache.skywalking.oap.server.core.query.enumeration.Order;
import org.apache.skywalking.oap.server.core.query.input.MetaQueryCondition;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsLogRes;
import org.apache.skywalking.oap.server.library.module.ModuleManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Slf4j
public class MetaQuery implements GraphQLQueryResolver {
    private final ModuleManager moduleManager;
    private MetaQueryService metaQueryService;

    public MetaQuery(ModuleManager moduleManager) {
        this.moduleManager = moduleManager;
    }

    private MetaQueryService getQueryService() {
        if (metaQueryService == null) {
            this.metaQueryService = moduleManager.find(CoreModule.NAME).provider().getService(MetaQueryService.class);
        }
        return metaQueryService;
    }

    public MetaEsLogRes queryMetaLogs(MetaQueryCondition condition) {
        String start = "";
        String end = "";
        if (nonNull(condition.getQueryDuration())) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
                Date startDate = sdf.parse(condition.getQueryDuration().getStart());
                Date endDate = sdf.parse(condition.getQueryDuration().getEnd());
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                start = sdf.format(startDate);
                end = sdf.format(endDate);
            } catch (ParseException e) {
                log.error("query meta logs parse date error");
                return new MetaEsLogRes();
            }
        }

        Order queryOrder = isNull(condition.getQueryOrder()) ? Order.DES : condition.getQueryOrder();

        return getQueryService().queryLogs(
                condition.getGroup(),
                condition.getPaging().getPageNum(),
                condition.getPaging().getPageSize(),
                condition.getSearchValue(),
                start, end,
                queryOrder);
    }
}
