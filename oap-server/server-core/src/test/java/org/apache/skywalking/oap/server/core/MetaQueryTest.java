package org.apache.skywalking.oap.server.core;

import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.oap.server.core.query.MetaQueryService;
import org.apache.skywalking.oap.server.core.query.enumeration.Order;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsLogRes;
import org.junit.Test;

import java.text.ParseException;

@Slf4j
public class MetaQueryTest {
    @Test
    public void test() throws ParseException {
        MetaQueryService metaesQueryService = new MetaQueryService();
        String start = "2021-09-01 00:00:00";
        String end = "2023-12-01 00:00:00";
        Order order = Order.DES;
        MetaEsLogRes iomp = metaesQueryService.queryLogs("PMTS", 1, 100, "Info", start, end, order);
        log.info("asdf");
    }
}
