package org.apache.skywalking.oap.server.core.query.type.meta;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Hits {
        private long total;
        @SerializedName("max_score")
        private boolean maxScore;
        private List<Hit> hits;

    }