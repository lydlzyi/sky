package org.apache.skywalking.oap.server.core.query.type.meta;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetaEsData {
    private String took;
    @SerializedName("timed_out")
    private boolean timeOut;
    @SerializedName("_shards")
    private Object shards;
    private Hits hits;
}
