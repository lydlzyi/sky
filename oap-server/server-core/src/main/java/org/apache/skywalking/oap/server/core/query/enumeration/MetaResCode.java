package org.apache.skywalking.oap.server.core.query.enumeration;

public enum MetaResCode {
    SUCCESS("BDOS0000"),
    UNAUTH("BDOS1002"),
    ;

    public final String code;
    MetaResCode(String code) {
        this.code = code;
    }
}
