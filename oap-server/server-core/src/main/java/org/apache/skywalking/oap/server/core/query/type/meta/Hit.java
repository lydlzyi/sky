package org.apache.skywalking.oap.server.core.query.type.meta;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hit {
    @SerializedName("_index")
    private String index;
    @SerializedName("_type")
    private String type;
    @SerializedName("_id")
    private String id;

    @SerializedName("_score")
    private Object score;
    @SerializedName("_source")
    private MetaEsLog source;
    private Object sort;
}