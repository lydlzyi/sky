package org.apache.skywalking.oap.server.core.query;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import io.netty.handler.codec.http.HttpHeaderValues;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.skywalking.oap.server.core.query.enumeration.MetaConfigKey;
import org.apache.skywalking.oap.server.core.query.enumeration.MetaResCode;
import org.apache.skywalking.oap.server.core.query.enumeration.Order;
import org.apache.skywalking.oap.server.core.query.type.meta.Hit;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsLogRes;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsLog;
import org.apache.skywalking.oap.server.core.query.type.meta.Hits;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsReq;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaLoginReq;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsParamReq;
import org.apache.skywalking.oap.server.core.query.type.meta.MetaEsData;
import org.apache.skywalking.oap.server.library.module.Service;
import org.apache.skywalking.oap.server.library.util.CollectionUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

@Slf4j
public class MetaQueryService implements Service {
    private String token = "token";

    public MetaEsLogRes queryLogs(String group, int from, int limit, String value, String start, String end, Order queryOrder) {
        MetaEsLogRes res = new MetaEsLogRes();
        Gson gson = new Gson();
        // get config
        JsonObject configObj = this.getConfigFromFile(gson);

        if (configObj == null) {
            log.error("configObj is null");
            return res;
        }

        String defaultIp = "";
        String backUpIp = "";
        String port = "";
        JsonElement defaultIpElement = configObj.get(MetaConfigKey.DEFAULT.key);
        if (defaultIpElement != null) defaultIp = defaultIpElement.getAsString();
        JsonElement backupIpElement = configObj.get(MetaConfigKey.BACKUP.key);
        if (backupIpElement != null) backUpIp = backupIpElement.getAsString();
        JsonElement portElement = configObj.get(MetaConfigKey.PORT.key);
        if (portElement != null) port = portElement.getAsString();

        if (StringUtils.isEmpty(defaultIp) || StringUtils.isEmpty(backUpIp) || StringUtils.isEmpty(port)) {
            log.error("config file is invalid");
            return res;
        }

        // get dsl
        String dsl = this.getDSL(group, from, limit, value, configObj, start, end, queryOrder);
        log.info("METAES query dsl: " + dsl);
        MetaEsReq metaesReq = this.getMetaesReq(configObj);
        if (metaesReq == null) {
            log.error("metaesReq is null");
            return res;
        }
        metaesReq.setDsl(dsl);

        Map<String, Object> resMap = this.getMetaEsData(defaultIp, backUpIp, port, gson, false, configObj, metaesReq);
        if (resMap.isEmpty() || resMap.get("data") == null) {
            log.error("get METAES data fail response is empty");
            return res;
        }

        String dataJson = (String) resMap.get("data");
        MetaEsData metaEsDataRes = gson.fromJson(dataJson, MetaEsData.class);
        Hits hits = metaEsDataRes.getHits();
        List<Hit> hitList = hits.getHits();
        if (CollectionUtils.isEmpty(hitList))
            return res;
        List<MetaEsLog> list = new ArrayList<>();
        for (Hit hit : hitList) list.add(hit.getSource());
        res.setTotal(hits.getTotal());
        res.setList(list);
        return res;
    }

    private String getDSL(String group, int from, int limit, String value, JsonObject configObj, String start, String end, Order queryOrder) {
        JsonElement selectKeyElement = configObj.get(MetaConfigKey.SELECT_KEY.key);
        String key = "logdata";
        if (selectKeyElement != null)
            key = selectKeyElement.getAsString();

        JsonElement groupKeyElement = configObj.get(MetaConfigKey.GROUP_KEY.key);
        String groupKey = "appcode";
        if (groupKeyElement != null)
            groupKey = groupKeyElement.getAsString();

        JsonElement sortKeyElement = configObj.get(MetaConfigKey.SORT_KEY.key);
        String sortKey = "logtime";
        if (sortKeyElement != null)
            sortKey = sortKeyElement.getAsString();

        JsonElement dateKeyElement = configObj.get(MetaConfigKey.DATE_KEY.key);
        String dateKey = "logtime";
        if (dateKeyElement != null)
            dateKey = dateKeyElement.getAsString();

        StringBuilder query = new StringBuilder();
        query.append("{");
        this.buildQueryDSL(query, groupKey, key, dateKey, group, value, start, end);
        this.buildSortDSL(query, sortKey, Order.DES.name().equals(queryOrder.name()) ? "desc" : "asc");
        this.buildLimitDSL(query, from, limit);
        query.append("}");
        return query.toString();
    }

    private void buildLimitDSL(StringBuilder query, int from, int limit) {
        query.append("\"from\":").append((from - 1) * limit).append(",");
        query.append("\"size\":").append(limit);
    }

    private void buildSortDSL(StringBuilder query, String sortKey, String sortValue) {
        query.append("\"sort\":{");
        query.append("\"").append(sortKey).append("\":\"").append(sortValue).append("\"");
        query.append("},");
    }

    private void buildQueryDSL(StringBuilder query, String groupKey, String key, String dateKey, String group, String value, String start, String end) {
        query.append("\"query\":{");
        query.append("\"bool\":{");
        this.buildQueryBuildDSL(query, groupKey, key, group, value);
        if (StringUtils.isNotEmpty(start) && StringUtils.isNotEmpty(end)) {
            query.append(",");
            this.buildQueryFilterDSL(query, dateKey, start, end);
        }
        query.append("}");
        query.append("},");
    }

    private void buildQueryFilterDSL(StringBuilder query, String dateKey, String start, String end) {
        query.append("\"filter\":[");
        query.append("{");
        query.append("\"range\":{");
        query.append("\"").append(dateKey).append("\":{");
        query.append("\"gte\":\"").append(start).append("\",");
        query.append("\"lte\":\"").append(end).append("\"");
        query.append("}");
        query.append("}");
        query.append("}");
        query.append("]");
    }

    private void buildQueryBuildDSL(StringBuilder query, String groupKey, String key, String group, String value) {
        query.append("\"must\":[");
        query.append("{");
        query.append("\"match_phrase\":{");
        query.append("\"").append(groupKey).append("\":\"").append(group).append("\"");
        query.append("}");
        query.append("}");

        if (StringUtils.isEmpty(value)) {
            query.append("]");
            return;
        }
        query.append(",");
        query.append("{");
        query.append("\"match_phrase\":{");
        query.append("\"").append(key).append("\":\"").append(value).append("\"");
        query.append("}");
        query.append("}");
        query.append("]");
    }

    private String getToken(String ip, String port, CloseableHttpClient httpClient, Gson gson, JsonObject configObj) {
        JsonElement tokenApiElement = configObj.get(MetaConfigKey.TOKEN_API.key);
        if (tokenApiElement == null) {
            log.error("METAES config is invalid not find token");
            return "";
        }
        String tokenApi = tokenApiElement.getAsString();

        MetaLoginReq metaLoginReq = this.getMetaLoginReq(configObj);
        if (metaLoginReq == null) return "";

        String url = ip + ":" + port + tokenApi;
        HttpPost post = new HttpPost(url);
        post.setHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON.toString());

        CloseableHttpResponse httpResponse = null;

        try {
            StringEntity entity = new StringEntity(gson.toJson(metaLoginReq));
            post.setEntity(entity);
            httpResponse = httpClient.execute(post);
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine != null && statusLine.getStatusCode() == HttpStatus.SC_OK) {
                String entityJson = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                Map<String, Object> map = gson.fromJson(entityJson, Map.class);
                Map<String, Object> dataMap = (Map<String, Object>) map.get("data");
                return (String) dataMap.get("token");
            } else {
                log.error("get METAES token fail, url:" + url);
                return "";
            }
        } catch (Exception e) {
            log.error("get METAES token error", e);
            return "";
        } finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    private MetaLoginReq getMetaLoginReq(JsonObject configObj) {
        JsonElement usernameElement = configObj.get(MetaConfigKey.USERNAME.key);
        if (usernameElement == null) {
            log.error("METAES config is invalid not find username");
            return null;
        }
        String userName = usernameElement.getAsString();

        JsonElement passwordElement = configObj.get(MetaConfigKey.PASSWORD.key);
        if (passwordElement == null) {
            log.error("METAES config is invalid not find password");
            return null;
        }
        String password = passwordElement.getAsString();

        MetaLoginReq metaLoginReq = new MetaLoginReq();
        metaLoginReq.setPassword(password);
        metaLoginReq.setUsername(userName);
        return metaLoginReq;
    }

    private Map<String, Object> getMetaEsData(String defaultIp,
                                              String backupIp,
                                              String port,
                                              Gson gson,
                                              boolean isRetry,
                                              JsonObject configObj,
                                              MetaEsReq metaesReq
    ) {
        JsonElement getApiElement = configObj.get(MetaConfigKey.GET_API.key);
        if (getApiElement == null) {
            log.error("configObj is invalid cannot get getApi");
            return new HashMap<>();
        }
        String getApi = getApiElement.getAsString();
        String defaultUrl = defaultIp + ":" + port + getApi;

        CloseableHttpClient httpClient = HttpClients.custom().build();
        try {
            HttpPost post = new HttpPost(defaultUrl);
            post.setHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON.toString());
            post.setHeader("X-token", token);

            CloseableHttpResponse httpResponse = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(gson.toJson(metaesReq), StandardCharsets.UTF_8);
                post.setEntity(entity);
                httpResponse = httpClient.execute(post);
                StatusLine statusLine = httpResponse.getStatusLine();
                if (statusLine == null || statusLine.getStatusCode() != HttpStatus.SC_OK) {
                    log.error("get METAES data fail url={}", defaultUrl);
                    return new HashMap<>();
                }

                String responseStr = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
                Map<String, Object> resMap = gson.fromJson(responseStr, Map.class);
                String code = (String) resMap.get("code");
                if (MetaResCode.SUCCESS.code.equals(code)) return resMap;
                if (MetaResCode.UNAUTH.code.equals(code)) {
                    if (isRetry) {
                        log.error("get METAES from url:" + defaultUrl + " fail token is invalid");
                        return new HashMap<>();
                    }
                    // get
                    this.token = this.getToken(defaultIp, port, httpClient, gson, configObj);
                    if (StringUtils.isEmpty(token)) {
                        log.error("get METAES toekn fail request url:" + defaultIp);
                        return new HashMap<>();
                    }

                    return this.getMetaEsData(defaultIp, backupIp, port, gson, true, configObj, metaesReq);
                }

                log.error("get METAES data fail url={}", defaultUrl);
                return new HashMap<>();
            } catch (Exception e) {
                log.error("get METAES data error to " + defaultUrl, e);
                return this.getMetaEsDataByBackup(backupIp, port, getApi, entity, httpClient, gson, false, configObj);
            } finally {
                if (httpResponse != null) {
                    try {
                        httpResponse.close();
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    private MetaEsReq getMetaesReq(JsonObject configObj) {
        JsonElement apiKeyElement = configObj.get(MetaConfigKey.API_KEY.key);
        if (apiKeyElement == null) {
            log.error("configObj is invalid cannot get apiKey");
            return null;
        }
        String apiKey = apiKeyElement.getAsString();

        JsonElement indexElement = configObj.get(MetaConfigKey.INDEX.key);
        if (indexElement == null) {
            log.error("configObj is invalid cannot get INDEX");
            return null;
        }
        String index = indexElement.getAsString();

        JsonElement dsCodeElement = configObj.get(MetaConfigKey.DS_CODE.key);
        if (dsCodeElement == null) {
            log.error("configObj is invalid cannot get DSCODE");
            return null;
        }
        String dsCode = dsCodeElement.getAsString();

        MetaEsReq metaesReq = new MetaEsReq();
        metaesReq.setApikey(apiKey);
        metaesReq.setDscode(dsCode);
        MetaEsParamReq metaesParamReq = new MetaEsParamReq();
        metaesParamReq.setIndex(index + "*");
        metaesReq.setParam(metaesParamReq);
        return metaesReq;
    }

    private Map<String, Object> getMetaEsDataByBackup(String backupIp, String port, String getApi, StringEntity entity, CloseableHttpClient httpClient, Gson gson, boolean isRetry, JsonObject configObj) {
        String backupUrl = backupIp + ":" + port + getApi;
        HttpPost post = new HttpPost(backupUrl);
        post.setHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON.toString());
        post.setHeader("X-token", token);
        post.setEntity(entity);

        CloseableHttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(post);
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine == null || statusLine.getStatusCode() != HttpStatus.SC_OK) {
                log.error("get METAES data fail url={}", backupUrl);
                return new HashMap<>();
            }

            String responseStr = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            Map<String, Object> resMap = gson.fromJson(responseStr, Map.class);
            String code = (String) resMap.get("code");
            if (MetaResCode.SUCCESS.code.equals(code)) return resMap;
            if (MetaResCode.UNAUTH.code.equals(code)) {
                if (isRetry) {
                    log.error("get METAES from url:" + backupUrl + " fail token is invalid");
                    return new HashMap<>();
                }

                this.token = this.getToken(backupIp, port, httpClient, gson, configObj);
                if (StringUtils.isEmpty(token)) {
                    log.error("get METAES toekn fail request url:" + backupUrl);
                    return new HashMap<>();
                }

                return this.getMetaEsDataByBackup(backupIp, port, getApi, entity, httpClient, gson, true, configObj);
            } else {
                log.error("get METAES data fail url={},res={}", backupUrl, responseStr);
                return new HashMap<>();
            }
        } catch (Exception e) {
            log.error("get METAES data error to " + backupUrl, e);
            return new HashMap<>();
        } finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    private JsonObject getConfigFromFile(Gson gson) {
        JsonReader reader = null;
        try {
            reader = new JsonReader(new FileReader("/opt/skywalking/oap-core/meta_api.json"));
            return gson.fromJson(reader, JsonObject.class);
        } catch (FileNotFoundException e) {
            log.error("get config from meta_api.json error");
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error("error, close reader error", e);
                }
            }
        }
    }
}
