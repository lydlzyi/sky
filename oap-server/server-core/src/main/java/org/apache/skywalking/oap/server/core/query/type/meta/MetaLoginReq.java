package org.apache.skywalking.oap.server.core.query.type.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetaLoginReq {
    private String username;
    private String password;
}
