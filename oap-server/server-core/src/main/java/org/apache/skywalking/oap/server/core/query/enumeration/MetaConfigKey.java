package org.apache.skywalking.oap.server.core.query.enumeration;

public enum MetaConfigKey {
    DEFAULT("default"),
    BACKUP("backup"),
    PORT("port"),
    TOKEN_API("tokenApi"),
    GET_API("getApi"),
    USERNAME("username"),
    PASSWORD("password"),
    API_KEY("apiKey"),
    INDEX("index"),
    DS_CODE("dscode"),
    SELECT_KEY("selectKey"),
    GROUP_KEY("groupKey"),
    SORT_KEY("sortKey"),
    DATE_KEY("dateKey"),
    ;

    public final String key;
    MetaConfigKey(String key) {
        this.key = key;
    }
}
