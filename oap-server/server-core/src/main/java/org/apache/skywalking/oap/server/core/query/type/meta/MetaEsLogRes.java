package org.apache.skywalking.oap.server.core.query.type.meta;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MetaEsLogRes {
    private long total = 0;
    private List<MetaEsLog> list = new ArrayList<>();
}
