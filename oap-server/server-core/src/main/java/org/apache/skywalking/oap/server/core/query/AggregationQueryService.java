/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.skywalking.oap.server.core.query;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.oap.server.core.Const;
import org.apache.skywalking.oap.server.core.analysis.IDManager;
import org.apache.skywalking.oap.server.core.analysis.manual.instance.InstanceTraffic;
import org.apache.skywalking.oap.server.core.query.input.Duration;
import org.apache.skywalking.oap.server.core.query.input.TopNCondition;
import org.apache.skywalking.oap.server.core.query.type.KeyValue;
import org.apache.skywalking.oap.server.core.query.type.SelectedRecord;
import org.apache.skywalking.oap.server.core.storage.StorageModule;
import org.apache.skywalking.oap.server.core.storage.annotation.ValueColumnMetadata;
import org.apache.skywalking.oap.server.core.storage.query.IAggregationQueryDAO;
import org.apache.skywalking.oap.server.library.module.ModuleManager;
import org.apache.skywalking.oap.server.library.module.Service;
import org.apache.skywalking.oap.server.library.util.StringUtil;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AggregationQueryService implements Service {
    private final ModuleManager moduleManager;
    private IAggregationQueryDAO aggregationQueryDAO;

    public AggregationQueryService(ModuleManager moduleManager) {
        this.moduleManager = moduleManager;
    }

    private IAggregationQueryDAO getAggregationQueryDAO() {
        if (aggregationQueryDAO == null) {
            aggregationQueryDAO = moduleManager.find(StorageModule.NAME)
                    .provider()
                    .getService(IAggregationQueryDAO.class);
        }
        return aggregationQueryDAO;
    }

    public List<SelectedRecord> sortMetrics(TopNCondition condition, Duration duration) throws IOException {
        int topN = condition.getTopN();
        condition.setTopN(100);
        String app = condition.getGroup();
        final String valueCName = ValueColumnMetadata.INSTANCE.getValueCName(condition.getName());
        List<KeyValue> additionalConditions = null;
        if (StringUtil.isNotEmpty(condition.getParentService())) {
            additionalConditions = new ArrayList<>(1);
            final String serviceId = IDManager.ServiceID.buildId(condition.getParentService(), condition.isNormal());
            additionalConditions.add(new KeyValue(InstanceTraffic.SERVICE_ID, serviceId));
        }
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader("/opt/skywalking/oap-core/services.json"));
        JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);
        List<SelectedRecord> list = new ArrayList<>();
        final List<SelectedRecord> selectedRecords = getAggregationQueryDAO().sortMetrics(
                condition, valueCName, duration, additionalConditions);
        selectedRecords.forEach(selectedRecord -> {
            switch (condition.getScope()) {
                case Service:
                    String name = IDManager.ServiceID.analysisId(selectedRecord.getId()).getName();
                    selectedRecord.setName(name);

                    if (StringUtil.isEmpty(app)) {
                        list.add(selectedRecord);
                    } else {
                        JsonElement nameElement = jsonObject.get(name);
                        if (nameElement == null) {
                            log.error("sortMetrics get config from file is null config name={}", name);
                        } else {
                            if (jsonObject.get(name).getAsString().equals(app)) {
                                list.add(selectedRecord);
                            }
                        }
                    }
                    break;
                case ServiceInstance:
                    final IDManager.ServiceInstanceID.InstanceIDDefinition instanceIDDefinition
                            = IDManager.ServiceInstanceID.analysisId(selectedRecord.getId());
                    /*
                     * Add the service name into the name if this is global top N.
                     */
                    if (StringUtil.isEmpty(condition.getParentService())) {
                        IDManager.ServiceID.ServiceIDDefinition serviceIDDefinition =
                                IDManager.ServiceID.analysisId(instanceIDDefinition.getServiceId());
                        name = serviceIDDefinition.getName();
                        selectedRecord.setName(serviceIDDefinition.getName() + " - " + instanceIDDefinition.getName());

                        if (StringUtil.isEmpty(app)) {
                            list.add(selectedRecord);
                        } else {
                            JsonElement nameElement = jsonObject.get(name);
                            if (nameElement == null) {
                                log.error("sortMetrics get config from file is null config name={}", name);
                            } else {
                                if (jsonObject.get(name).getAsString().equals(app)) {
                                    list.add(selectedRecord);
                                }
                            }
                        }
                    } else {
                        selectedRecord.setName(instanceIDDefinition.getName());
                        list.add(selectedRecord);
                    }
                    break;
                case Endpoint:
                    final IDManager.EndpointID.EndpointIDDefinition endpointIDDefinition
                            = IDManager.EndpointID.analysisId(selectedRecord.getId());
                    /*
                     * Add the service name into the name if this is global top N.
                     */
                    if (StringUtil.isEmpty(condition.getParentService())) {
                        IDManager.ServiceID.ServiceIDDefinition serviceIDDefinition =
                                IDManager.ServiceID.analysisId(endpointIDDefinition.getServiceId());
                        name = serviceIDDefinition.getName();
                        selectedRecord.setName(serviceIDDefinition.getName()
                                + " - " + endpointIDDefinition.getEndpointName());

                        if (StringUtil.isEmpty(app)) {
                            list.add(selectedRecord);
                        } else {
                            JsonElement nameElement = jsonObject.get(name);
                            if (nameElement == null) {
                                log.error("sortMetrics get config from file is null config name={}", name);
                            } else {
                                if (jsonObject.get(name).getAsString().equals(app)) {
                                    list.add(selectedRecord);
                                }
                            }
                        }
                    } else {
                        selectedRecord.setName(endpointIDDefinition.getEndpointName());
                        list.add(selectedRecord);
                    }
                    break;
                default:
                    selectedRecord.setName(Const.UNKNOWN);
                    list.add(selectedRecord);
            }
        });

        if (list.size() > topN) {
            return list.subList(0, topN);
        }
        return list;
    }
}
