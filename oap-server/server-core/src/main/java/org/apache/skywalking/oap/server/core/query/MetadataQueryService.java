/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.skywalking.oap.server.core.query;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import org.apache.skywalking.oap.server.core.Const;
import org.apache.skywalking.oap.server.core.analysis.IDManager;
import org.apache.skywalking.oap.server.core.analysis.NodeType;
import org.apache.skywalking.oap.server.core.query.type.Database;
import org.apache.skywalking.oap.server.core.query.type.Endpoint;
import org.apache.skywalking.oap.server.core.query.type.EndpointInfo;
import org.apache.skywalking.oap.server.core.query.type.Service;
import org.apache.skywalking.oap.server.core.query.type.ServiceInstance;
import org.apache.skywalking.oap.server.core.storage.StorageModule;
import org.apache.skywalking.oap.server.core.storage.query.IMetadataQueryDAO;
import org.apache.skywalking.oap.server.library.module.ModuleManager;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MetadataQueryService implements org.apache.skywalking.oap.server.library.module.Service {

    private final ModuleManager moduleManager;

    private IMetadataQueryDAO metadataQueryDAO;

    public MetadataQueryService(ModuleManager moduleManager) {
        this.moduleManager = moduleManager;
    }

    private IMetadataQueryDAO getMetadataQueryDAO() {
        if (metadataQueryDAO == null) {
            metadataQueryDAO = moduleManager.find(StorageModule.NAME).provider().getService(IMetadataQueryDAO.class);
        }
        return metadataQueryDAO;
    }

    public List<Service> getAllServices(final String group) throws IOException {

        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader("/opt/skywalking/oap-core/group.json"));
        JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);

        List<Service> list = getMetadataQueryDAO().getAllServices(Const.EMPTY_STRING).stream()
                .peek(service -> {
                    service.setGroup(Const.EMPTY_STRING);
                    if (group == null || group.length() == 0) {
                        for (Object o : jsonObject.entrySet()) {
                            Map.Entry entry = (Map.Entry) o;
                            String key = entry.getKey().toString();
                            JsonArray array = jsonObject.getAsJsonArray(key);
                            for (int i = 0; i < array.size(); i++) {
                                if (array.get(i).getAsString().equals(service.getName())) {
                                    service.setGroup(key);
                                    break;
                                }
                            }
                        }
                    } else {
                        JsonArray array = jsonObject.getAsJsonArray(group);
                        if (array != null) {
                            array.forEach(item -> {
                                if (item.getAsString().equals(service.getName())) {
                                    service.setGroup(group);
                                }
                            });
                        }
                    }
                })
                .distinct()
                .collect(Collectors.toList());
        if (group != null && group.length() > 0) {
            list.removeIf(e -> !group.equals(e.getGroup()));
        }
        return list;
    }

    public List<Service> getAllBrowserServices() throws IOException {
        return getMetadataQueryDAO().getAllBrowserServices().stream().distinct().collect(Collectors.toList());
    }

    public List<Database> getAllDatabases() throws IOException {
        return getMetadataQueryDAO().getAllDatabases().stream().distinct().collect(Collectors.toList());
    }

    public List<Service> searchServices(final long startTimestamp, final long endTimestamp,
                                        final String keyword) throws IOException {
        return getMetadataQueryDAO().searchServices(NodeType.Normal, keyword)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<Service> searchBrowserServices(final long startTimestamp, final long endTimestamp,
                                               final String keyword) throws IOException {
        return getMetadataQueryDAO().searchServices(NodeType.Browser, keyword)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }

    public List<ServiceInstance> getServiceInstances(final long startTimestamp, final long endTimestamp,
                                                     final String serviceId) throws IOException {
        return getMetadataQueryDAO().getServiceInstances(startTimestamp, endTimestamp, serviceId)
                .stream().distinct().collect(Collectors.toList());
    }

    public List<Endpoint> searchEndpoint(final String keyword, final String serviceId,
                                         final int limit) throws IOException {
        return getMetadataQueryDAO().searchEndpoint(keyword, serviceId, limit)
                .stream().distinct().collect(Collectors.toList());
    }

    public Service searchService(final String serviceCode) throws IOException {
        return getMetadataQueryDAO().searchService(NodeType.Normal, serviceCode);
    }

    public Service searchBrowserService(final String serviceCode) throws IOException {
        return getMetadataQueryDAO().searchService(NodeType.Browser, serviceCode);
    }

    public EndpointInfo getEndpointInfo(final String endpointId) throws IOException {
        final IDManager.EndpointID.EndpointIDDefinition endpointIDDefinition = IDManager.EndpointID.analysisId(
                endpointId);
        final IDManager.ServiceID.ServiceIDDefinition serviceIDDefinition = IDManager.ServiceID.analysisId(
                endpointIDDefinition.getServiceId());

        EndpointInfo endpointInfo = new EndpointInfo();
        endpointInfo.setId(endpointId);
        endpointInfo.setName(endpointIDDefinition.getEndpointName());
        endpointInfo.setServiceId(endpointIDDefinition.getServiceId());
        endpointInfo.setServiceName(serviceIDDefinition.getName());
        return endpointInfo;
    }
}
