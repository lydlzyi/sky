package org.apache.skywalking.oap.server.core.query.type.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetaEsLog {
    private String puttime;
    private String filenode;
    private String logdata;
    private String syscode;
    private String logtype;
    private String collno;
    private String filename;
    private String machine;
    private String gettime;
    private String nodecode;
    private String filedir;
    private String appcode;
    private String logtime;
    private String msgtype;
}
